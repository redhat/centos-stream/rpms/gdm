From 1580f05a56b671950a23d267c815a977cd56655b Mon Sep 17 00:00:00 2001
From: =?UTF-8?q?Florian=20M=C3=BCllner?= <fmuellner@gnome.org>
Date: Wed, 28 Jun 2023 13:55:01 +0200
Subject: [PATCH] daemon: Specify DCONF_PROFILE for initial-setup

Since commit edc1815175, the initial-setup session no longer uses
the locked down "gdm" profile, but the regular user profile.

On the one hand this seems justified, given that the initial-setup
session is different enough from the login screen, however it is
also different from a regular user session:

 - screen locking isn't useful for a system user
 - during setup, log out / user switching are pointless at best
 - accessibility features should be readily available in case
   the user needs them

For this reason, return to specify DCONF_PROFILE for the initial-setup
user, but use a distince profile from "gdm" to allow initial-setup
to provide its own presets.
---
 daemon/gdm-launch-environment.c | 6 +-----
 1 file changed, 1 insertion(+), 5 deletions(-)

diff --git a/daemon/gdm-launch-environment.c b/daemon/gdm-launch-environment.c
index 507d6de74..5e379bf85 100644
--- a/daemon/gdm-launch-environment.c
+++ b/daemon/gdm-launch-environment.c
@@ -155,71 +155,67 @@ build_launch_environment (GdmLaunchEnvironment *launch_environment,
                 "LC_TIME",
                 "LD_LIBRARY_PATH",
                 "PATH",
                 "WINDOWPATH",
                 "XCURSOR_PATH",
                 "XDG_CONFIG_DIRS",
                 NULL
         };
         char *system_data_dirs;
         g_auto (GStrv) supported_session_types = NULL;
         int i;
 
         /* create a hash table of current environment, then update keys has necessary */
         hash = g_hash_table_new_full (g_str_hash, g_str_equal, g_free, g_free);
 
         for (i = 0; optional_environment[i] != NULL; i++) {
                 if (g_getenv (optional_environment[i]) == NULL) {
                         continue;
                 }
 
                 g_hash_table_insert (hash,
                                      g_strdup (optional_environment[i]),
                                      g_strdup (g_getenv (optional_environment[i])));
         }
 
         if (launch_environment->x11_authority_file != NULL)
                 g_hash_table_insert (hash, g_strdup ("XAUTHORITY"), g_strdup (launch_environment->x11_authority_file));
 
         if (launch_environment->session_mode != NULL) {
                 g_hash_table_insert (hash, g_strdup ("GNOME_SHELL_SESSION_MODE"), g_strdup (launch_environment->session_mode));
+                g_hash_table_insert (hash, g_strdup ("DCONF_PROFILE"), g_strdup (launch_environment->user_name));
 
 		if (strcmp (launch_environment->session_mode, INITIAL_SETUP_SESSION_MODE) != 0) {
 			/* gvfs is needed for fetching remote avatars in the initial setup. Disable it otherwise. */
 			g_hash_table_insert (hash, g_strdup ("GVFS_DISABLE_FUSE"), g_strdup ("1"));
 			g_hash_table_insert (hash, g_strdup ("GIO_USE_VFS"), g_strdup ("local"));
 			g_hash_table_insert (hash, g_strdup ("GVFS_REMOTE_VOLUME_MONITOR_IGNORE"), g_strdup ("1"));
-
-			/* The locked down dconf profile should not be used for the initial setup session.
-			 * This allows overridden values from the user profile to take effect.
-			 */
-			g_hash_table_insert (hash, g_strdup ("DCONF_PROFILE"), g_strdup ("gdm"));
 		}
         }
 
         g_hash_table_insert (hash, g_strdup ("LOGNAME"), g_strdup (launch_environment->user_name));
         g_hash_table_insert (hash, g_strdup ("USER"), g_strdup (launch_environment->user_name));
         g_hash_table_insert (hash, g_strdup ("USERNAME"), g_strdup (launch_environment->user_name));
 
         g_hash_table_insert (hash, g_strdup ("GDM_VERSION"), g_strdup (VERSION));
         g_hash_table_remove (hash, "MAIL");
 
         g_hash_table_insert (hash, g_strdup ("HOME"), g_strdup ("/"));
         g_hash_table_insert (hash, g_strdup ("PWD"), g_strdup ("/"));
         g_hash_table_insert (hash, g_strdup ("SHELL"), g_strdup ("/bin/sh"));
 
         gdm_get_pwent_for_name (launch_environment->user_name, &pwent);
         if (pwent != NULL) {
                 if (pwent->pw_dir != NULL && pwent->pw_dir[0] != '\0') {
                         g_hash_table_insert (hash, g_strdup ("HOME"), g_strdup (pwent->pw_dir));
                         g_hash_table_insert (hash, g_strdup ("PWD"), g_strdup (pwent->pw_dir));
                 }
 
                 g_hash_table_insert (hash, g_strdup ("SHELL"), g_strdup (pwent->pw_shell));
         }
 
         if (start_session && launch_environment->x11_display_seat_id != NULL) {
                 char *seat_id;
 
                 seat_id = launch_environment->x11_display_seat_id;
 
                 g_hash_table_insert (hash, g_strdup ("GDM_SEAT_ID"), g_strdup (seat_id));
-- 
2.41.0.rc2

